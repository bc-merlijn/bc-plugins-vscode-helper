import { promises as fs, PathLike } from "fs";
import * as path from "path";
import { PackageInfo, VersionLevel } from "./types"
import { bumpVersionNumber } from "./bumpVersionNumber"

export class PackageTraverser {
  packages: any[];
  dependencySet: Set<string>;
  dependenciesChecked: Set<string>;
  dependencyMap: Map<string, any>;
  dependenciesToModify: Set<string>;
  targetPackageName: string;

  constructor(allPackages: any[], targetPackageInfo: PackageInfo) {
    this.packages = allPackages;
    this.dependencySet = new Set();
    this.dependencyMap = new Map();
    this.dependenciesChecked = new Set();

    this.dependenciesToModify = new Set();
    this.dependenciesToModify.add(targetPackageInfo.name);
    this.targetPackageName = targetPackageInfo.name;
  }

  async updateDepedenciesOf(
      targetPlugins: any[] = [],
      level: VersionLevel,
      jiraId: string,
      changelogEntryDescription: string
    ): Promise<any[]> {
    for (let plugin of targetPlugins) {
      this.dependencySet.add(plugin);
    }
    
    // Get all nested dependencies for the targetPlugins (e.g. s3 / sftp)
    for (let dependency of this.dependencySet) {
      if (this.dependenciesChecked.has(dependency)) {
        continue;
      }
      const { localDependencies, version } = this.getLocalDependencies(dependency);
      this.dependencyMap.set(dependency, {
        localDependencies,
        version,
        newVersion: bumpVersionNumber(
          version.split(".").map(level => parseInt(level, 10)),
          level
        ).join(".")
      });
      localDependencies.forEach(localDependency => {
        this.dependencySet.add(localDependency);
      });
      this.dependenciesChecked.add(dependency);
    }

    // Determine which ones are relevant for the targetPackage (e.g. taskrunner)
    for (let dependency of this.dependenciesToModify) {
      for (let [key, dependencies] of this.dependencyMap) {
        if (dependencies.localDependencies.includes(dependency)) {
          this.dependenciesToModify.add(key);
        }
      }
    }

    const fileModificationPromises = [];
    for (let {location, package: name} of this.packages) {
      if (!this.dependenciesToModify.has(name)) {
        continue;
      }

      const packageBumpPromise = this.bumpPackageJsonFile(
        path.join(location, "package.json"),
        level,
        targetPlugins.includes(name),
        name
      );
      
      fileModificationPromises.push(
        packageBumpPromise.then((packageBumpResult) => {
          return this.updateChangelogFile(
            path.join(location, "CHANGELOG.md"),
            packageBumpResult,
            targetPlugins.includes(packageBumpResult.packageName),
            jiraId,
            changelogEntryDescription
          )
        })
      );
      // this.addChangelogEntry(filePath);
    }

    const results = await Promise.all(fileModificationPromises);

    return results;

    /**
     * TODO:
     * - Loop through the array above
     * - Get file path of every package listed, EXCEPT:
     *   - Target package (e.g. taskrunner or blueconicservice)
     * - Get matching CHANGELOG.md file path.
     * - Open & edit all files in parallel.
     *   - Bump version for all package.json files EXCEPT: Target plugins (e.g. s3 / sftp)
     *   - Add changelog entry
     * - Resolve when done.
     */
  }

  private getLocalDependencies(targetPlugin: string) {
    const localPackage = this.packages.find(packageObject => packageObject?.package === targetPlugin);
    if (localPackage?.packageContents?.dependencies) {
      return {
        version: localPackage?.packageContents?.version,
        localDependencies: Object.keys(localPackage.packageContents.dependencies).filter(dependency => dependency.startsWith("@blueconic/"))
      }
    }
    return {
      version: "",
      localDependencies: []
    }
  }

  private async bumpPackageJsonFile(
    filePath: PathLike,
    level: VersionLevel,
    isTargetPlugin: boolean = false,
    packageName: string
  ) {
    const packageVersionRegex = /"version": "v?([0-9]+)\.([0-9]+)\.([0-9]+)(.*)"/;
    const dependenciesBlockRegex = /"dependencies": {(\n.*)+?\n\s*},/g;
    const dependencyList =
      Array
        .from(this.dependenciesToModify)
        .map(dependency => dependency.replace(/@blueconic\//, ""))
        .join("|")
    const dependencyRegex = new RegExp(`"@blueconic\\/(${dependencyList})": "[\\^\\~]?v?([0-9]+|x|\\*)(?:\\.([0-9]+|x))?(?:\\.([0-9]+|x))?(?:-.*)?"`, "g");

    let fileContents = await fs.readFile(filePath, {encoding: "utf-8"});

    // Update the package version.
    const versionMatches = fileContents.match(packageVersionRegex);
    let newVersion = "";
    let oldVersion = "";
    if (versionMatches && versionMatches.length > 0 && !isTargetPlugin) {
      const [fullMatch, major, minor, patch, ext] = versionMatches;
      oldVersion = [major, minor, patch].join(".");
      newVersion = bumpVersionNumber(
        [major, minor, patch].map(level => parseInt(level, 10)),
        level
      ).join(".");
      fileContents = fileContents.replace(packageVersionRegex, `"version": "${newVersion}"`)
    }

    // Update the package dependencies.
    const dependenciesBlockMatch = fileContents.match(dependenciesBlockRegex);
    if (dependenciesBlockMatch && dependenciesBlockMatch.length > 0) {
      let dependencies = dependenciesBlockMatch[0]
      dependencies = dependencies.replace(dependencyRegex, (match, dependencyName) => {
        const version = this.dependencyMap.get(`@blueconic/${dependencyName}`).newVersion;
        return `"@blueconic/${dependencyName}": "${version}"`
      });
      fileContents = fileContents.replace(dependenciesBlockRegex, dependencies);
    }

    await fs.writeFile(filePath, fileContents);

    return {
      oldVersion,
      newVersion,
      packageName
    };
  }

  private async updateChangelogFile(
    filePath: PathLike,
    packageBumpResult,
    isTargetPlugin: boolean = false,
    jiraId: string,
    changelogEntryDescription: string
  ) {
    if (!isTargetPlugin) {
      let fileContents = await fs.readFile(filePath, {encoding: "utf-8"});
      const currentDate = (new Date()).toISOString().split('T')[0];
      const changelogEntry = 
        `\n## ${packageBumpResult.newVersion} - ${currentDate}` +
        "\n" +
        "\n### Changed" +
        "\n" +
        `\n- [${jiraId}](https://jira.blueconic.com/jira/browse/${jiraId}) - ${changelogEntryDescription}` +
        "\n" +
        "\n##";
      fileContents = fileContents.replace("\n##", changelogEntry);
      
      await fs.writeFile(filePath, fileContents);
    }

    return packageBumpResult
  }
}