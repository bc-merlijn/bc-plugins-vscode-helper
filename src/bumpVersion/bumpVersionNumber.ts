import { VERSION_LEVELS } from "./enum"
import { VersionLevel } from "./types";

export function bumpVersionNumber(semverArray: number[], level: VersionLevel) {
  const [major, minor, patch] = semverArray;
  switch(level) {
    case VERSION_LEVELS.MAJOR:
      return [major + 1,0,0];
    case VERSION_LEVELS.MINOR:
      return [major, minor + 1, 0];
    case VERSION_LEVELS.PATCH:
      return [major, minor, patch + 1];
  }
}