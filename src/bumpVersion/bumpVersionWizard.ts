import * as vscode from "vscode";
import * as semver from "semver";
import { getPackages } from "../getPackages";
import { PackageTraverser } from "./PackageTraverser";
import { PackageInfo, VersionLevel } from "./types";
import { bumpVersionNumber } from "./bumpVersionNumber";
import { VERSION_LEVELS } from "./enum";

export async function bumpPackageVersionNumber(context: vscode.Uri) {
  let allLernaPackages = [];
  const haveAllPackages = getPackages((result) => {
    allLernaPackages = allLernaPackages.concat(result);
  });

  if (!context) {
    vscode.window.showWarningMessage(
      "No context given for this command! You should trigger it from the context menu instead."
    );
    return;
  }

  const level = (await vscode.window.showQuickPick(
    Object.values(VERSION_LEVELS).reverse(),
    {
      placeHolder: VERSION_LEVELS.PATCH,
    }
  )) as VersionLevel;

  if (!level) {
    // Abort
    return;
  }

  await haveAllPackages;

  const packageInfo = await getInfoOfPackage(context.path, level);
  if (packageInfo === false) {
    vscode.window.showErrorMessage("Could not read current package.json");
    return;
  }

  const rootPaths = vscode.workspace.workspaceFolders?.map(
    (folder) => folder.uri.path
  );
  console.warn(rootPaths);
  const certifiedPlugins = allLernaPackages.filter(
    (plugin) =>
      plugin.package &&
      (plugin.location.includes(`${rootPaths[0]}/plugins/certified/`) ||
        plugin.location.includes(`${rootPaths[0]}/apps/`))
  );

  const targetPlugins = await vscode.window.showQuickPick(
    certifiedPlugins.map((plugin) => plugin.package),
    {
      canPickMany: true,
      ignoreFocusOut: true,
    }
  );

  if (!targetPlugins || targetPlugins.length === 0) {
    // Abort
    return;
  }

  const jiraId = await vscode.window.showInputBox({
    prompt: "Jira issue identifier",
    value: getJiraIdFromBranchName(),
  });

  if (!jiraId) {
    // Abort
    return;
  }

  const changelogEntry = await vscode.window.showInputBox({
    prompt: "Changelog description",
  });

  bumpCurrentPackage(context.path, packageInfo);

  const packageTraverser = new PackageTraverser(allLernaPackages, packageInfo);
  const modifiedPackages = await packageTraverser.updateDepedenciesOf(
    targetPlugins,
    level,
    jiraId,
    changelogEntry
  );
  const modifiedPackagesFormatted = modifiedPackages.reduce(
    (acc, modifiedPackage, index) => {
      const isLast = index === modifiedPackages.length - 1;
      acc = `${acc} ${modifiedPackage.packageName}${isLast ? "" : "; "}`;
      return acc;
    },
    ""
  );

  vscode.window.showInformationMessage(
    `Successfully modified the package.json of the following packages: ${modifiedPackagesFormatted}`
  );
}

function getCurrentBranchName(): string {
  // Get current branch name
  const gitExtension = vscode.extensions.getExtension("vscode.git").exports;
  const gitApi = gitExtension.getAPI(1);

  const repo = gitApi.repositories[0];
  const head = repo.state.HEAD;

  return head.name;
}

const jiraIdRegex = /BC-[0-9]+/g;
function getJiraIdFromBranchName() {
  const matches = getCurrentBranchName().match(jiraIdRegex);
  let jiraId = "";
  if (matches && matches.length > 0) {
    jiraId = matches[matches.length - 1];
  }
  return jiraId;
}

const packageVersionRegex = /"version": "v?([0-9]+)\.([0-9]+)\.([0-9]+)(.*)"/;
const packageNameRegex = /"name": "(@blueconic\/[a-zA-Z0-9\-]+)"/;
/**
 *
 * @param jsonPath - The path of the current package.json we wish to bump the version of
 * @param level - Which version level to bump: "MAJOR", "MINOR" or "PATCH" (default)
 * @returns
 */
async function getInfoOfPackage(
  jsonPath: string,
  level: VersionLevel = VERSION_LEVELS.PATCH
): Promise<PackageInfo | false> {
  const activeEditor = vscode.window.activeTextEditor;
  if (activeEditor && jsonPath === activeEditor.document.uri.path) {
    try {
      const packageText = await activeEditor.document.getText();
      const versionMatch = packageText.match(packageVersionRegex);
      const [, nameMatch] = packageText.match(packageNameRegex);
      if (versionMatch && nameMatch) {
        const currentVersion = [
          versionMatch[1],
          versionMatch[2],
          versionMatch[3],
        ].map((value) => parseInt(value, 10));
        const newVersion = bumpVersionNumber(currentVersion, level);
        return {
          currentVersion,
          newVersion,
          name: nameMatch,
          versionMatch,
        };
      } else {
        return false;
      }
    } catch {
      return false;
    }
  } else {
    return false;
  }
}

async function bumpCurrentPackage(jsonPath, { newVersion, versionMatch }) {
  const activeEditor = vscode.window.activeTextEditor;
  if (activeEditor && jsonPath === activeEditor.document.uri.path) {
    const range = new vscode.Range(
      activeEditor.document.positionAt(versionMatch.index),
      activeEditor.document.positionAt(
        versionMatch.index + versionMatch[0].length
      )
    );
    const newVersionString = newVersion.join(".");
    await activeEditor.edit((editBuilder) => {
      editBuilder.replace(
        range,
        `"version": "${newVersionString}${versionMatch[4]}"`
      );
    });
  }
}
