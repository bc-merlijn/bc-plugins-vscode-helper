import { VersionLevel } from "./types"

export const VERSION_LEVELS = {
  MAJOR: "MAJOR" as VersionLevel,
  MINOR: "MINOR" as VersionLevel,
  PATCH: "PATCH" as VersionLevel
}