export type PackageInfo = { 
  currentVersion: number[],
  newVersion: number[],
  name: string,
  versionMatch: RegExpMatchArray
}

export type VersionLevel = "MAJOR" | "MINOR" | "PATCH";
