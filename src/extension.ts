"use strict";

import * as vscode from "vscode";
import * as path from "path";
import { promisify } from "util";
import { exec } from "child_process";

import { DepNodeProvider, Dependency } from "./nodeDependencies";
import { FileSystemProvider } from "./fileExplorer";
import { getPackages, getPackageFile } from "./getPackages";
import { bumpPackageVersionNumber } from "./bumpVersion/bumpVersionWizard";
import { VERSION_LEVELS } from "./bumpVersion/enum";
import * as semver from "semver";

const WORKSPACE_ROOTPATH = vscode.workspace.rootPath;
let parentPath;
let currentModulePath;
function createFileExplorerForDep(
  packagePath: string,
  version: string,
  modulePath: string
) {
  const treeDataProvider = new FileSystemProvider(
    vscode.Uri.parse(packagePath)
  );

  vscode.window.registerTreeDataProvider(
    "dependencyFilesExplorer",
    treeDataProvider
  );
  const internalPackageView = vscode.window.createTreeView(
    "dependencyFilesExplorer",
    { treeDataProvider }
  );
  const pathArray = packagePath.split("/");
  internalPackageView.title = modulePath; // `${internalPackageView.title } (${pathArray.slice(0).slice(-3).join('/')}) (${version})`
  currentModulePath = modulePath;
  vscode.commands.registerCommand("dependencyFilesExplorer.getDiff", (file) =>
    getDiff(file)
  );
}
function getDiff(file: any) {
  console.log("gettingdiffremotepath", currentModulePath);
  const fileName = path.parse(file.path).base;
  vscode.commands.executeCommand(
    "vscode.diff",
    vscode.Uri.file(path.join(currentModulePath, fileName)),
    vscode.Uri.file(file.path)
  );
}
function focusSelectedFolder(uri: string) {
  const treeDataProvider = new FileSystemProvider(vscode.Uri.parse(uri));
  const view = vscode.window.createTreeView("srcFolderSelector", {
    treeDataProvider,
  });
  const pathArray = uri.split("/");
  view.title = `${view.title} (${pathArray.slice(1).slice(-3).join("/")})`;
  parentPath = uri
    .split("/")
    .slice(0, pathArray.length - 1)
    .join("/");
}

function goToParent() {
  focusSelectedFolder(parentPath);
}

function goToRoot() {
  focusSelectedFolder(WORKSPACE_ROOTPATH);
}

function createDependencyTreeForFolder({ uri }, allLernaPackages) {
  console.log(uri);
  const treeDataProvider = new DepNodeProvider(uri.path, allLernaPackages);
  vscode.window.registerTreeDataProvider("nodeDependencies", treeDataProvider);
  const view = vscode.window.createTreeView("nodeDependencies", {
    treeDataProvider,
  });
  const { name, version } = getPackageFile(uri.path);
  view.title = `${view.title} for (${name}) (${version})`;
  vscode.commands.registerCommand("nodeDependencies.refreshEntry", () =>
    treeDataProvider.refresh()
  );
  vscode.commands.registerCommand(
    "nodeDependencies.deleteEntry",
    (node: Dependency) =>
      vscode.window.showInformationMessage(
        `Successfully called delete entry on ${node.label}.`
      )
  );
  vscode.commands.registerCommand(
    "nodeDependencies.openPackageOnNpm",
    (moduleName) =>
      vscode.commands.executeCommand(
        "vscode.open",
        vscode.Uri.parse(`https://www.npmjs.com/package/${moduleName}`)
      )
  );
  vscode.commands.registerCommand(
    "nodeDependencies.openPackageLocal",
    (localPackagePath, version, modulePath) =>
      createFileExplorerForDep(localPackagePath, version, modulePath)
  );
}

export function activate(context: vscode.ExtensionContext) {
  // let allLernaPackages = [];
  // getPackages((result) => {
  //   allLernaPackages = allLernaPackages.concat(result);
  // });
  // focusSelectedFolder(WORKSPACE_ROOTPATH);
  // vscode.commands.registerCommand("srcFolderSelector.showDependecies", (uri) =>
  //   createDependencyTreeForFolder(uri, allLernaPackages)
  // );
  // vscode.commands.registerCommand(
  //   "srcFolderSelector.pinFolder",
  //   (uri: vscode.Uri) => focusSelectedFolder(uri.path)
  // );
  // vscode.commands.registerCommand("srcFolderSelector.goToRoot", () =>
  //   goToRoot()
  // );
  // vscode.commands.registerCommand("srcFolderSelector.goToParent", () =>
  //   goToParent()
  // );

  // Bump version
  vscode.commands.registerCommand(
    "srcFolderSelector.bumpVersion",
    (context: vscode.Uri) => bumpPackageVersionNumber(context)
  );

  // Show latest versions of local packages
  const isLatestDecorationType = vscode.window.createTextEditorDecorationType({
    backgroundColor: "#00FF0022",
  });
  const minorUpdateDecorationType =
    vscode.window.createTextEditorDecorationType({
      backgroundColor: "#FFAA0033",
    });
  const majorUpdateDecorationType =
    vscode.window.createTextEditorDecorationType({
      backgroundColor: "#FF000022",
    });

  const cwd = getCwd(vscode.window.activeTextEditor.document.fileName);
  getFreshLernaJson(cwd);

  context.subscriptions.push(
    vscode.window.onDidChangeActiveTextEditor((editor) =>
      addVersionsOfLocalPackages(
        editor,
        isLatestDecorationType,
        minorUpdateDecorationType,
        majorUpdateDecorationType
      )
    ),
    vscode.workspace.onDidChangeTextDocument((event) => {
      const editor = vscode.window.activeTextEditor;
      if (editor && event.document === editor.document) {
        addVersionsOfLocalPackages(
          editor,
          isLatestDecorationType,
          minorUpdateDecorationType,
          majorUpdateDecorationType
        );
      }
    })
  );
}

let allPackages: Record<
  string,
  {
    name: string;
    version: string;
    private: boolean;
    location: string;
  }
> = {};
let lastUpdate = 0;
const UPDATE_INTERVAL = 1000 * 60 * 5; // 5 minutes
const execPromise = promisify(exec);

async function getFreshLernaJson(cwd: string) {
  if (lastUpdate + UPDATE_INTERVAL < Date.now()) {
    lastUpdate = Date.now();
    try {
      console.log("CURRENT WORKING DIRECTORY", cwd);
      const { stdout: json } = await execPromise("yarn lerna ls --all --json", {
        encoding: "utf-8",
        cwd,
      });
      const allPackagesJson = JSON.parse(json);
      allPackages = allPackagesJson.reduce((acc, packageJson) => {
        acc[packageJson.name] = packageJson;
        return acc;
      });
    } catch (error) {
      console.error("Failed to parse lerna json", error);
    }
  }
}

function getCwd(fileName) {
  return (
    vscode.workspace.workspaceFolders?.[0]?.uri.fsPath ||
    fileName.substring(0, fileName.indexOf("plugins"))
  );
}

async function addVersionsOfLocalPackages(
  editor: vscode.TextEditor,
  isLatestDecorationType,
  minorUpdateDecorationType,
  majorUpdateDecorationType
) {
  if (editor?.document.fileName.endsWith(".json")) {
    const cwd = getCwd(editor.document.fileName);
    await getFreshLernaJson(cwd);
    // loop through lines and store the ones starting with "@blueconic/"
    const onLatest = [];
    const minorUpdate = [];
    const majorUpdate = [];
    for (let line = 0; line < editor.document.lineCount; line++) {
      const text = editor.document.lineAt(line).text;
      if (text.startsWith(`    "@blueconic/`)) {
        const packageName = text.substring(5, text.indexOf('"', 5));
        const localPackage = allPackages[packageName];
        if (localPackage) {
          const versionStringStart = text.indexOf(': "') + 3;
          const versionStringEnd = text.lastIndexOf('"');
          const versionInFile = text.substring(
            versionStringStart,
            versionStringEnd
          );
          const latestVersion = localPackage.version;
          const range = new vscode.Range(
            line,
            versionStringStart - 1,
            line,
            versionStringEnd + 1
          );
          const linkToFile = `\n\n[${localPackage.location.substring(
            cwd.length
          )}](${localPackage.location}/package.json)`;
          if (versionInFile === latestVersion) {
            onLatest.push({
              range,
              hoverMessage: `You are on the latest version available on this branch!${linkToFile}`,
            });
          } else if (semver.valid(semver.coerce(versionInFile))) {
            if (semver.major(versionInFile) === semver.major(latestVersion)) {
              minorUpdate.push({
                range,
                hoverMessage: `Latest version is ${latestVersion}${linkToFile}`,
              });
            } else {
              majorUpdate.push({
                range,
                hoverMessage: `Latest version is ${latestVersion}${linkToFile}`,
              });
            }
          }
        }
      }
      editor.setDecorations(isLatestDecorationType, onLatest);
      editor.setDecorations(minorUpdateDecorationType, minorUpdate);
      editor.setDecorations(majorUpdateDecorationType, majorUpdate);
    }
  }
}
