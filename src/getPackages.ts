import * as vscode from "vscode";
import * as fs from "fs";
import * as path from "path";
import * as loadJsonFile from "load-json-file";
import * as glob from "glob";

const PACKAGE_FILE = "package.json";
const LERNA_FILE = "lerna.json";

export const getPackageFile = (packagePath) => {
  const pkgJsonPath = path.join(packagePath, PACKAGE_FILE);
  if (fs.existsSync(pkgJsonPath)) {
    return loadJsonFile.sync(pkgJsonPath) as any;
  }
};

const loadPackage = (packagePath) => {
  const pkgJsonPath = path.join(packagePath, PACKAGE_FILE);
  if (fs.existsSync(pkgJsonPath)) {
    return loadJsonFile.sync(pkgJsonPath) as any;
  }
};

const findPackages = (packageSpecs, rootDirectory, cb) => {
  const promises = [];
  for (const spec of packageSpecs) {
    promises.push(
      new Promise((resolve) => {
        const globPath = path.join(rootDirectory, spec);
        glob(globPath, {}, (er, files) => {
          cb(getPackageNameAndLocation(files));
          resolve(files);
        });
      })
    );
  }
  return Promise.all(promises);
};

const getPackageNameAndLocation = (files: any[]) => {
  try {
    return files.map((location) => {
      const packageContents = loadPackage(location);
      return {
        location,
        package: packageContents?.name,
        packageContents: packageContents,
      };
    });
  } catch (err) {
    console.warn(err);
    return [];
  }
};

type LernaJson = {
  version: string;
  npmClient: string;
  useWorkspaces: boolean;
  command?: {
    publish: {
      ignoreChanges: string[];
      message: string;
      registry: string;
    };
    bootstrap: {
      ignore: string;
      npmClientArgs: string[];
    };
  };
  packages: string[];
};

export async function getPackages(cb: Function) {
  const directory = vscode.workspace.rootPath;
  const lernaJsonPath = path.join(directory, LERNA_FILE);
  if (fs.existsSync(lernaJsonPath)) {
    const lernaJson = loadJsonFile.sync(lernaJsonPath) as LernaJson;
    if (!lernaJson.useWorkspaces) {
      await findPackages(lernaJson.packages, directory, cb);
    }
  }

  const pkgJsonPath = path.join(directory, PACKAGE_FILE);
  if (fs.existsSync(pkgJsonPath)) {
    const pkgJson = loadJsonFile.sync(pkgJsonPath) as any;
    let workspaces = pkgJson.workspaces;

    if (pkgJson.bolt) {
      workspaces = pkgJson.bolt.workspaces;
    }

    if (workspaces) {
      if (Array.isArray(workspaces)) {
        await findPackages(workspaces, directory, cb);
      } else if (Array.isArray(workspaces.packages)) {
        await findPackages(workspaces.packages, directory, cb);
      }
    }
  }
}
